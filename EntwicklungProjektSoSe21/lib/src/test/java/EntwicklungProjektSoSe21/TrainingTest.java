package EntwicklungProjektSoSe21;



import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Test;


class TrainingTest {

	Training training = new Training();
	
	
	@Test
	void nameTest(){
		training.setTname("Fussball");
		Assert.assertEquals("Fussball", training.getTname());

	}
	
	@Test
	void beschreibungTest(){
		training.setBeschreibung("ballsport");
		Assert.assertEquals("ballsport", training.getBeschreibung());

	}

	@After
	public void cleanup() {
		training = null;	
	}
	
}
