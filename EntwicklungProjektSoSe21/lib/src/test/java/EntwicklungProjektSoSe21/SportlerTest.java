package EntwicklungProjektSoSe21;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

class SportlerTest {

Sportler sportler = new Sportler();
	
	
	@Test
	void nameTest(){
		sportler.setName("Klaus");
		Assert.assertEquals("Klaus", sportler.getName());

	}
	
	@Test
	void beschreibungTest(){
		sportler.setGr��e("184");
		Assert.assertEquals("184", sportler.getGr��e());

	}
	
	@Test
	void geschlechtTest(){
		sportler.setGeschlecht("m");
		Assert.assertEquals("m", sportler.getGeschlecht());

	}

	@After
	public void cleanup() {
		sportler = null;	
	}

}
