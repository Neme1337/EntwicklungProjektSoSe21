package EntwicklungProjektSoSe21;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javassist.expr.NewArray;

@Service
public class TrainingService {
	
		@Autowired
		private TrainingRespository trainingRepository;
	
		public List<Training> getTrainingList(){
			
			Iterator<Training>it = trainingRepository.findAll().iterator();
			ArrayList<Training> myList = new ArrayList<>();
			while (it.hasNext()) 
				myList.add(it.next());	
			
			
			return myList;
			
		}
		
		public Training getTraining(String tname) {
		        return trainingRepository.findById(tname).orElse(null);
		 }
		
		public void addTraining(Training training) {
			trainingRepository.save(training);	
		}
		
		public void updateTraining(String tNr, Training training) {
			trainingRepository.save(training);
		}
		
		public void deleteTraining(String tNr) {
			trainingRepository.deleteById(tNr);
		}
		
}
