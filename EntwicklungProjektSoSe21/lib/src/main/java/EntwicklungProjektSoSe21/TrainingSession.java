package EntwicklungProjektSoSe21;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;




@Entity
public class TrainingSession {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long sessionnr;

	private String sessionDate;
	private String sessionStart;
	private String sessionEnd;
	private String sessionGewicht;

	@ManyToOne
	private Training training;
	
	@ManyToOne
	@JsonIgnore
	private Sportler sportler;
	


	public TrainingSession() {}

	public TrainingSession(long sessionnr, String sessionDate, String sessionStart, String sessionEnd, String sessionGewicht, Training training, Sportler sportler) {
		super();
		System.out.println("asdasdasd");
		this.sessionnr = sessionnr;
		this.sessionDate = sessionDate;
		this.sessionStart = sessionStart;
		this.sessionEnd = sessionEnd;
		this.sessionGewicht = sessionGewicht;
		this.training = training;
		this.sportler = sportler;
	}

	
	
	
	public Training getTraining() {
		return training;
	}


	public void setTraining(Training training) {
		this.training = training;
	}


	public String getSessionDate() {
		return sessionDate;
	}


	public void setSessionDate(String sessionDate) {
		this.sessionDate = sessionDate;
	}


	public Sportler getSportler() {
		return sportler;
	}


	public void setSportler(Sportler sportler) {
		this.sportler = sportler;
	}
	
	
	
	
}
