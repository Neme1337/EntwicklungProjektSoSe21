package EntwicklungProjektSoSe21;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;




@Entity
public class Sportler {
	
	
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private long id;
	@Id
	private String name;
	private String geschlecht;
	private String gr��e;
	
	@OneToMany (mappedBy = "sportler", cascade=CascadeType.ALL)
	private List<TrainingSession> trainingSession;


	public Sportler() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Sportler (String name, String geschlecht, String gr��e) {
		super();
		this.setName(name);
		this.setGeschlecht(geschlecht);
		this.setGr��e(gr��e);
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGeschlecht() {
		return geschlecht;
	}
	public void setGeschlecht(String geschlecht) {
		this.geschlecht = geschlecht;
	}
	public String getGr��e() {
		return gr��e;
	}
	public void setGr��e(String gr��e) {
		this.gr��e = gr��e;
	}
	
	public List<TrainingSession> getTrainingSession() {
		System.out.println("getTrainingSession");
		return trainingSession;
	}
	public void setTrainingSession(List<TrainingSession> trainingSession) {
		for (TrainingSession session : trainingSession) {
			session.setSportler(this);
		}
			
		this.trainingSession = trainingSession;
	}
	
	public void addTrainingSession(TrainingSession trainingSession) {
		System.out.println("addTrainingSession");
		this.trainingSession.add(trainingSession);
	}
}
