package EntwicklungProjektSoSe21;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Training {
	
	@Id
	private String tname;
	
	private String beschreibung;
	
	@OneToMany(mappedBy="training")
	private List<TrainingSession> TrainingSession;

	public Training() {}
	public Training(String tname, String beschreibung) {
		super();
		this.tname = tname;
		this.beschreibung = beschreibung;
	}
	
	public String getTname() {
		return tname;
	}
	public void setTname(String tname) {
		this.tname = tname;
	}
	public String getBeschreibung() {
		return beschreibung;
	}
	public void setBeschreibung(String bezeichnung) {
		this.beschreibung = bezeichnung;
	}
//	public List<TrainingSession> getTrainingSession() {
//		return session;
//	}
//	private void setTrainingSession(List<TrainingSession> TrainingSession) {
//		this.session = TrainingSession;
//	}
	
	
	
	
}
