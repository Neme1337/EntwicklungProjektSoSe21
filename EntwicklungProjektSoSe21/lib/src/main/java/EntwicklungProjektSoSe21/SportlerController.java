package EntwicklungProjektSoSe21;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SportlerController {
	
	@Autowired 
	SportlerService sportlerService;

	@RequestMapping("/sportler/{name}")
	public Sportler getSportlerList(@PathVariable String name){
		return sportlerService.getSportler(name);
		
	}
	
	@RequestMapping("/sportler")
	public List<Sportler> getSportlersList(){
		return sportlerService.getSportlerList();
		
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/sportler")
	public void addSportler(@RequestBody Sportler sportler) {
		sportlerService.addSportler(sportler);
		
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/sportler/{name}")
	public void updateSportler(@PathVariable String name,@RequestBody Sportler sportler) {
		sportlerService.updateSportler(sportler, name);
		
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/sportler/{name}")
	public void deleteSportler(@PathVariable String name) {
		sportlerService.deleteSportler(name);

	}
}