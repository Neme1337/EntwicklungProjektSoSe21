package EntwicklungProjektSoSe21;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;


@RestController
public class TrainingController {
	
		@Autowired
		TrainingService trainingService;
	
		@RequestMapping("/training")
		public List<Training> getTrainingList(){
			return trainingService.getTrainingList();
			
		}
		
		@RequestMapping("/training/{tname}")
		public Training getTraining(@PathVariable String tname){
			return trainingService.getTraining(tname);
		}
		
		@RequestMapping(method=RequestMethod.POST, value="/training")
		public void addTraining(@RequestBody Training training) {
			trainingService.addTraining(training);
		}
		
		@RequestMapping(method=RequestMethod.PUT, value="/training/{tname}")
		public void updateTraining(@PathVariable String tname,@RequestBody Training training) {
			trainingService.updateTraining(tname, training);
		}
		
		@RequestMapping(method=RequestMethod.DELETE, value="/training/{tname}")
		public void deleteTraining(@PathVariable String tname) {
			trainingService.deleteTraining(tname);
		}
		
	
}
